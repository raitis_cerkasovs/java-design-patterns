package myAdapter;

public interface MediaPlayer {
	
	public void play(String audioType, String fileName);

}
