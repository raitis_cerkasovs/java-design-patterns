package myComposite;

import java.util.ArrayList;
import java.util.List;

interface Thing {
	void print();
}

class Something implements Thing {	
	String name;

	Something(String name) {
		this.name = name;
	}
	
	@Override
	public void print() {
       System.out.println("My name is: " + this.name);		
	}	
}

class Somethings implements Thing {
	
	List<Thing> list = new ArrayList<Thing>();

	@Override
	public void print() {
		for (Thing thing : this.list)	
			thing.print();
	}
	
	/// may no need
	void add(Thing thing) {
		this.list.add(thing);
	}
}

public class Run {
 public static void main(String[] args) {
    Thing thing1 = new Something("Seperate thing 1");
    thing1.print();    
    Thing thing2 = new Something("Seperate thing 2");
    thing2.print();
    Somethings thingList = new Somethings();
    thingList.add(thing1);
    thingList.add(thing2);
    thingList.print(); 
 }
}
