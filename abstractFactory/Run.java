package abstractFactory;

interface Shape {
	void drow();
}

class Rectangle implements Shape {
	@Override
	public void drow() {
        System.out.println("Rectangle");		
	}	
}

class Circle implements Shape {
	@Override
	public void drow() {
        System.out.println("Circle");		
	}	
}

interface Color {
	void fill();
}

class Red implements Color {
	@Override
	public void fill() {
       System.out.println("Red");
	}	
}

class Green implements Color {
	@Override
	public void fill() {
       System.out.println("Green");
	}	
}

class Blue implements Color {
	@Override
	public void fill() {
       System.out.println("Red");
	}	
}

abstract class AbstractFactory {
	abstract Color getColor(String color);
	abstract Shape getShape(String shape);
}

class ShapeFactory extends AbstractFactory {

	@Override
	Color getColor(String colorType) {
		return null;
	}

	@Override
	Shape getShape(String shapeType) {

      if (shapeType == null)
		return null;
      
	  if (shapeType.equals("circle"))
		return new Circle();  
	   
	  if (shapeType.equals("rectangle"))
		return new Rectangle();
		
	  return null;
	}
} 
	
	
class ColorFactory extends AbstractFactory {

	@Override
    Color getColor(String colorType) {
			
	   if (colorType == null)
		  return null;
		       
	   if (colorType.equals("red"))
		  return new Red();  
			   
	   if (colorType.equals("green"))
		  return new Green();
	   return null;
	}

	@Override
	Shape getShape(String shapeType) {
	   return null;
    }
}

		
class FactoryProducer {
	
	static AbstractFactory getFactory(String choice) {
		if (choice.equals("shape"))
			return new ShapeFactory();
		else if (choice.equals("color"))
			return new ColorFactory();
		
	  return null;
	}
}				

public class Run {
    public static void main(String[] args) {
    	AbstractFactory shapeFactory = FactoryProducer.getFactory("shape");
    	AbstractFactory colorFactory = FactoryProducer.getFactory("color");
    	
    	Shape s = shapeFactory.getShape("rectangle");
    	s.drow();
    	Color c = colorFactory.getColor("red");
    	c.fill();
    }
 }

