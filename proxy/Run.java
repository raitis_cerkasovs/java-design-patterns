package myProxy;

interface Image {
	void displey();
}

class Normal implements Image {
	
	Normal() {
		System.out.println("Load Image");
	}

	@Override
	public void displey() {
		System.out.println("Display Image");
	}	
}

class Proxy implements Image {
	
	Normal img;

	@Override
	public void displey() {
        if (img == null)
        	img = new Normal();
     img.displey();
	}	
}

public class Run {
	public static void main(String[] args) {
		
	       Image img1 = new Proxy();
	       img1.displey();
	       img1.displey();
	       
	       Image img2 = new Proxy();
	       img2.displey();
	       img2.displey();
	       
	       img1.displey();	       
    }
}
