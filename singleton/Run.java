package mySingleton;

final class Singleton {

  private Singleton() {}
  
  public String name;
  private  static Singleton instance;

  public static Singleton getInstance() {
	if (instance == null) 
		instance = new Singleton();
    return instance;
  }
}

public class Run {
	
	public static void main(String[] args) {

	    Singleton s = Singleton.getInstance();
	    s.name = "lala";
	    
	    s = Singleton.getInstance();	    
		System.out.println(s.name);
	}
}
