package myStrategy;

interface Movement {
	void move();
}

class Run implements Movement {
	@Override
	public void move() {
       System.out.println("Run");		
	}	
}

class Go implements Movement {
	@Override
	public void move() {
       System.out.println("Go");		
	}	
}

class Strategy {
	Movement i;
		
	Strategy(Movement i) {
		this.i = i;
	}
	
	void move() {
		i.move();
	}
}

public class Runer {
	
	public static void main(String[] args) {
		Strategy one = new Strategy(new Run());
		one.move();		
		
		Strategy two = new Strategy(new Go());
		two.move();
	}

}
