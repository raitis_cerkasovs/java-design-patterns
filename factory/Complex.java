package myFactory;
	
	import static java.lang.Math.cos;
	import static java.lang.Math.sin;
	
	public class Complex {
		 
	   public static Complex fromCartesian(double real, double imaginary) {
	      return new Complex(real, imaginary);
	   }
	   
	   public static Complex fromPolar(double modulus, double angle) {
	      return new Complex(modulus * cos(angle), modulus * sin(angle));
	   }
	   
	   private Complex(double a, double b) {}
	   
	   
	   
	  public static void main(String[] args) {
	    Complex c1 = Complex.fromPolar(1, 45);
	    Complex c2 = Complex.fromCartesian(5, 6);
	    
	    System.out.println(c1.toString());
	    System.out.println(c2.toString());
	 }
}


