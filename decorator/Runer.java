package MyDec2;

	
interface Fun {
		void run();
	}
	
class SomethingUsesFun implements Fun {
		@Override
		public void run() {
			 System.out.println("Fun");		
		}	
	}
	
abstract class AbstractDecorator implements Fun {
		protected Fun fun;
		public AbstractDecorator(Fun fun) {
			this.fun = fun;
		}
	}
	
class Decorator extends AbstractDecorator{
		public Decorator(Fun fun) {
			super(fun);
		}

		@Override
		public void run() {
                fun.run();
                System.out.println("decorated");
		}
}
	
public class Runer {
  public static void main(String[] args) {
	 Fun normalObject = new SomethingUsesFun();
	 normalObject.run();
	 
	 Fun decorated = new Decorator(new SomethingUsesFun());
	 decorated.run();
  }
}
