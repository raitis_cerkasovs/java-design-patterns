package myBridge;

public class RedCircle implements Draw {

	@Override
	public void drawCircle(int radius, int x, int y) {
        System.out.println("Red circle: " + x + " " + y + " " + radius);		
	}

}
