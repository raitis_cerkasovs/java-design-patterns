package myBridge;

public interface Draw {
	
	public void drawCircle(int radius, int x, int y);

}
