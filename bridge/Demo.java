package myBridge;

// DECOUPLE abstraction from its IMPLEMENTATION

public class Demo {

	public static void main(String[] args) {
        Shape redCircle = new Circle(10, 10, 4, new RedCircle());
        Shape greenCircle = new Circle(10, 10, 5, new GreenCircle());
        
        redCircle.drawMe();
        greenCircle.drawMe();
	}

}
