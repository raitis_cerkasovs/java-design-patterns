package factoryMethod;

class Room {}

class OrdinaryRoom extends Room {}

public class Game {
	
	public Room room;
		
	// call factory method from constructor
    public Game() {
        room = makeRoom();
    }
    
    // factory method
    protected Room makeRoom() {
        return new OrdinaryRoom();
    }
    
    public static void main(String[] args) {
    	Game game = new Game();
		System.out.println(game.room);
	}
}